$(document).ready(function () {
    new TypeIt('#nama-type', {
        strings: 'Muhammad Al-Kautsar Maktub',
        speed: 200,
        waitUntilVisible: true,
        loop: true,
        nextStringDelay: [500, 3000]
    }).go();
})
